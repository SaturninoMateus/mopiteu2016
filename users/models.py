from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings



class UserAddress(models.Model):
    '''This class represents the Address table on Database, where each
    user can have many address'''
    user = models.ForeignKey(User)

    address_name = models.CharField(max_length=25)
    receiver_first_name = models.CharField(max_length=25)
    receiver_last_name = models.CharField(max_length=25)

    state = models.CharField(max_length=20, choices=settings.STATE_CHOICES, default='LUANDA')
    district = models.CharField(max_length=25)
    street = models.CharField(max_length=120)
    number = models.PositiveIntegerField()
    reference = models.CharField(max_length=120)
    timestamp = models.DateTimeField(auto_now_add = True, auto_now=False)
    updated = models.DateTimeField(auto_now_add = False, auto_now=True)

    def __unicode__(self):
        return self.user.username
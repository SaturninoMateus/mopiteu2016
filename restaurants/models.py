from __future__ import unicode_literals
from django.db.models.signals import pre_save, post_save
from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.db.models.fields import TimeField
import datetime
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator




def create_slug_zone(instance, new_slug=None):
    slug = slugify(instance.zone)
    if new_slug is not None:
        slug = new_slug
    qs = Restaurant.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_slug_zone(instance, new_slug=new_slug)
    return slug

def create_slug_restaurant(instance, new_slug=None):
    slug = slugify(instance.name)
    if new_slug is not None:
        slug = new_slug
    qs = Restaurant.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_slug_restaurant(instance, new_slug=new_slug)
    return slug



class DeliveryZone(models.Model):
    zone = models.CharField(max_length=25, default=None)
    is_active = models.BooleanField(default=False)
    slug = models.SlugField(unique=True,editable=False)

    def __unicode__(self):
        return self.zone


def pre_save_deliveryzone_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug_zone(instance)

pre_save.connect(pre_save_deliveryzone_receiver, sender=DeliveryZone)

class RestaurantAddress(models.Model):

    '''This class represents the Address table on Database, where each
    restaurant can have many address'''

    restaurant = models.ForeignKey('Restaurant', related_name='restaurant')

    address_name = models.CharField(max_length=25)
    state = models.CharField(max_length=20, choices=settings.STATE_CHOICES, default='LUANDA')
    district = models.CharField(max_length=25)
    street = models.CharField(max_length=120)
    number = models.PositiveIntegerField()
    reference = models.CharField(max_length=120)
    timestamp = models.DateTimeField(auto_now_add = True, auto_now=False)
    updated = models.DateTimeField(auto_now_add = False, auto_now=True)


    def __unicode__(self):
        return self.restaurant.name

    def get_address(self):
        return "%s, %s, %s, %s "%(self.district, self.street, self.number, self.state)

def image_upload_to_restaurant(instance, filename):
    title = instance.name
    slug = slugify(title)
    basename, file_extension = filename.split(".")
    new_filename = "%s-%s.%s" %(slug, instance.id, file_extension)
    return "restaurants/%s/images/%s" %(slug, new_filename)

def image_upload_to_restaurant2(instance, filename):
    print 'entrou aqui no image_upload_to_restaurant2'
    title = instance.name
    slug = slugify(title+'2')
    basename, file_extension = filename.split(".")
    new_filename = "%s-%s.%s" %(slug, instance.id, file_extension)
    return "restaurants/%s/images/%s" %(slug, new_filename)

class Restaurant(models.Model):
    name = models.CharField(max_length=120)
    description = models.CharField(max_length=240, blank=True, null=True)
    email = models.EmailField(unique=True)
    is_active = models.BooleanField(default=False)
    is_open = models.BooleanField(default=False)
    slug = models.SlugField(unique=True,editable=False)
    service_type = models.CharField(max_length=25, choices=settings.SERVICE_TYPE_CHOICES, default='DELIVERY')
    delivery_zones = models.ManyToManyField('DeliveryZone', default=None)


    open_at = models.TimeField(default= datetime.time)
    close_at = models.TimeField(default=datetime.time)


    min_order = models.PositiveIntegerField() #min order total price
    delivery_tax = models.PositiveIntegerField() #delivery for tax
    delivery_free_from = models.PositiveIntegerField() #delivery free from specific value
    delivery_is_free = models.BooleanField(default=False)

    img_logo = models.ImageField(upload_to=image_upload_to_restaurant)
    img_banner = models.ImageField(upload_to=image_upload_to_restaurant2)
    phone = models.IntegerField(max_length=9, unique=True, validators=[RegexValidator(regex='^\d{9}$', message='Length has to be 9', code='Invalid number')])


    def __unicode__(self):
        return self.name

    def get_address(self):
        address = RestaurantAddress.objects.filter(restaurant = self).first()
        return address.get_address()




def pre_save_restaurant_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug_restaurant(instance)

pre_save.connect(pre_save_restaurant_receiver, sender=Restaurant)

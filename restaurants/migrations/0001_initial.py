# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-23 03:42
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Restaurant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120)),
                ('description', models.CharField(blank=True, max_length=240, null=True)),
                ('email', models.EmailField(max_length=254, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='RestaurantAddress',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address_name', models.CharField(max_length=25)),
                ('state', models.CharField(choices=[(b'LUANDA', b'Luanda'), (b'BENGUELA', b'Benguela')], default='LUANDA', max_length=20)),
                ('district', models.CharField(max_length=25)),
                ('street', models.CharField(max_length=120)),
                ('number', models.PositiveIntegerField()),
                ('reference', models.CharField(max_length=120)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('restaurant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='restaurant', to='restaurants.Restaurant')),
            ],
        ),
        migrations.AddField(
            model_name='restaurant',
            name='address',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='address', to='restaurants.RestaurantAddress'),
        ),
    ]

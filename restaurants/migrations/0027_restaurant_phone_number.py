# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-03-13 22:03
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('restaurants', '0026_remove_restaurant_phone_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='restaurant',
            name='phone_number',
            field=models.IntegerField(default=888888888, max_length=9, validators=[django.core.validators.RegexValidator(code='Invalid number', message='Length has to be 9', regex='^\\d{9}$')]),
            preserve_default=False,
        ),
    ]

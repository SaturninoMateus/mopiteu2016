from django.contrib import admin
from models import Restaurant, RestaurantAddress, DeliveryZone


admin.site.register(Restaurant)
admin.site.register(RestaurantAddress)
admin.site.register(DeliveryZone)

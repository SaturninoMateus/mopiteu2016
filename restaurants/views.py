from django.shortcuts import render, redirect
from django.views.generic import View,TemplateView
from restaurants.models import DeliveryZone
from django.shortcuts import get_object_or_404, get_list_or_404
from django.http import Http404
from django.conf import settings
from models import Restaurant, RestaurantAddress
from products.models import Product, Variation

class RestaurantHome(View):

    def post(self, request):
        print 'kkk'
        opZone = request.POST.get('opZone')
        opServiceType = request.POST.get('opServiceType')
        opOpened = request.POST.get('opOpened')

        print 'zone: ',opZone
        print 'servico: ',opServiceType
        print 'OpOpened: ', opOpened

        print opServiceType



        if opServiceType == 'Entrega':
            opServiceType = 'Delivery'

        elif opServiceType == 'Retirada':
            opServiceType = 'Pickup'

        elif opServiceType == 'Entrega & Retirada':
            opServiceType = 'DELIVERY&PICKUP'
            print opServiceType
            zones = DeliveryZone.objects.filter(zone__iexact=opZone,is_active=True,) #iexact = insensitive exact character
            print 'zones: ', zones


            if len(zones) > 0:
                restaurants = get_list_or_404(
                Restaurant.objects.order_by('-is_open'),
                    delivery_zones__in=zones,
                    is_active=True,
                )
            else:
                restaurants = []
            print restaurants

            params = {"restaurants":restaurants, "city":opZone.capitalize(),"service":opServiceType, "opOpened":opOpened}

            return render(request, 'restaurant/list_page.html', params)


        #Case Delivery or Pickup
        print opServiceType
        zones = DeliveryZone.objects.filter(zone__icontains=opZone,is_active=True,) #iexact = insensitive exact character
        print 'zones: ', zones
        restaurants = get_list_or_404(
                Restaurant.objects.order_by('-is_open'),
                service_type__icontains=opServiceType,
                delivery_zones__in=zones,
                is_active=True,

        )

        print restaurants
        params = {"restaurants":restaurants, "city":opZone.capitalize(), "service":opServiceType, "opOpened":opOpened}

        return render(request, 'restaurant/list_page.html', params)


##########################################################################
class RestaurantProducts(View):

    def get(self,request,id, slug):
        print 'kkkkkkkkkkkkkkkk'
        restaurant = get_object_or_404(Restaurant,slug=slug,id=id)
        #get a list of products from the given restaurant
        products = restaurant.product_set.all()
        currency = '$'
        params = {"restaurant":restaurant, "products":products,"currency":currency}

        #Usage on the template: product.variation_set.all()
        return render(request, 'restaurant/restaurant_products.html', params)
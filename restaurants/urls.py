from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from views import RestaurantHome, RestaurantProducts

urlpatterns = [
    url(r'^$', RestaurantHome.as_view(), name='restaurant'),
    url(r'^(?P<id>\d+)/(?P<slug>[\w-]+)/$', RestaurantProducts.as_view(), name='restaurant_products'),
]
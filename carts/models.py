from __future__ import unicode_literals

from django.conf import settings
from django.db import models

from products.models import Variation
from restaurants.models import Restaurant


class CartItem(models.Model):
    cart = models.ForeignKey("Cart")
    item = models.ForeignKey(Variation)
    quantity = models.PositiveIntegerField(default=1)
    #line_item_total ==

    def __unicode__(self):
        return self.item.title


class Cart(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)
    restaurant = models.ForeignKey(Restaurant)

    items = models.ManyToManyField(Variation, through=CartItem)
    timestamp = models.DateTimeField(auto_now_add=True,auto_now=False)
    updated = models.DateTimeField(auto_now_add=False,auto_now=True)


    def __unicode__(self):
        return str(self.id)

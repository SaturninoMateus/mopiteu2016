from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from views import CartView

urlpatterns = [
    url(r'^$', CartView.as_view(), name='cart'),
]
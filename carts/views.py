from django.shortcuts import render, redirect
from django.views.generic import View,TemplateView
from restaurants.models import DeliveryZone
from django.shortcuts import get_object_or_404, get_list_or_404
from django.http import Http404, HttpResponseRedirect
from django.conf import settings
from restaurants.models import Restaurant
from products.models import Variation, Product
from carts.models import Cart, CartItem


class CartView(View):

    def get(self,request, *args, **kwargs):
        print 'carrrrrrrrrrrrrrrrrrt'
        request.session.set_expiry(300*12*12) #300ms = 5 min, 300*12 = 1h, 300*12*12= 12h ,0 = when the user close the browser
        #prod_id = kwargs.get("prod_id")
        prod_id = request.GET.get("item")
        slug = kwargs.get("slug")
        #restaurant = Restaurant.objects.filter(slug=slug)[0]
        restaurant = get_object_or_404(Restaurant, slug=slug)

        ############################
        response = render(request, 'product/detail_page.html', {})
        ############################



        #cart_id = request.session.get("car_id")
        cart_id = request.COOKIES.get("car_id")


        print 'Cart id: ',cart_id
        if cart_id == None:
            cart = Cart()
            cart.restaurant = restaurant
            cart.save()
            cart_id = cart.id
            #request.session["cart_id"] = cart_id
            #request.session.save()

            response.set_cookie("cart_id", cart_id)

        cart = Cart.objects.get(id=cart_id)
        if request.user.is_authenticated():
            cart.user = request.user
            cart.save()
        item_id = request.GET.get("item")
        delete_item = request.GET.get("delete")


        if item_id:
            #Means new product in cart
            item_instance = get_object_or_404(Variation, id=item_id)
            qty = request.GET.get("qty")
            #cart = Cart.objects.get_or_create(restaurant=restaurant,user=request.user)[0]
            cart_item = CartItem.objects.get_or_create(cart=cart, item=item_instance)[0]
            if delete_item:
                cart_item.delete()
            else:
                cart_item.quantity = qty
                cart_item.save()
        return HttpResponseRedirect("/")
# coding: utf-8
from django.shortcuts import render, HttpResponseRedirect
from models import NewsletterModel
from django.views.generic import View
from django.core.mail import send_mail

class Newsletter(View):
    def post(self, request, *args, **kwargs):
        email = request.POST.get("email")

        if email:
            NewsletterModel.objects.get_or_create(email=email)
            msg1 = '''
            A sua subscrição foi confirmada com sucesso! Você será notificado(a) assim que o sistema estiver disponível.

            Atenciosamente,

            Serviço de Apoio ao Cliente MOPITEU.
                    '''
            subject1 = 'MOPITEU- Subscrição'

            subject2 = 'MOPITEU- Parceria'

            msg2 = '''
            O objetivo da Mopiteu é facilitar a difusão dos produtos oferecidos por lanchonetes e restaurantes. Temos também como missão, fazer com que os clientes tenham mais facilidade de requisitar refeições cujas entregas sejam feitas domiciliarmente e, consequentemente fazer com que se sintam mais seguros.
            Para mais informações por favor envie um email para: sac@mopiteu.com

            Atenciosamente,

            Serviço de Apoio ao Cliente MOPITEU.
                   '''


            send_mail(subject1, msg1,'sac@mopiteu.com', [email], fail_silently=False)
            send_mail(subject2, msg2,'sac@mopiteu.com', [email], fail_silently=False)

            return HttpResponseRedirect("/")

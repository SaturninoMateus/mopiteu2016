from __future__ import unicode_literals

from django.db import models

class NewsletterModel(models.Model):
    email = models.EmailField()

    def __unicode__(self):
        return self.email

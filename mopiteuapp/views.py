from django.shortcuts import render, redirect
from django.views.generic import View,TemplateView
from restaurants.models import DeliveryZone
from django.shortcuts import get_object_or_404, get_list_or_404
from django.http import Http404
from django.conf import settings

class Home(View):
    def get(self, request, *args, **kwargs):
        #send as a response to the request a list of Delivery Zones and Service types available in the system
        #zones =  DeliveryZone.objects.all().order_by('zone') #.order_by('zone') = ASC Zone and .order_by('-zone') = DESC Zone
        #zones =  DeliveryZone.objects.all().order_by('zone') #.order_by('zone') = ASC Zone and .order_by('-zone') = DESC Zone

        is_developer = request.GET.get("d")
        '''if not is_developer:
            return render(request, 'comingsoon/index.html', {})'''



        zones = get_list_or_404(DeliveryZone.objects.order_by('zone'), is_active=True)
        params = {"zones":zones, 'serviceTypes':settings.SERVICE_TYPE_CHOICES_LIST}

        ############################
        response = render(request, 'homeBeta1.html', params)
        response.set_cookie('teste','123')
        return response
        ############################

        #return render(request, 'homeBeta1.html', params)



    def post(self, request):
        opZone = request.POST.get('opZone')
        opServiceType = request.POST.get('opServiceType')


        return render(request, 'home.html',{})



class Contact(View):
    def get(self, request, *args, **kwargs):
        params = {}
        return render(request, 'contact/contacts.html', params)
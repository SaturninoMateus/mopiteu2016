from django.conf import settings
import os
import dj_database_url

DEBUG = False
TEMPLATE_DEBUG = False
DEBUG404 = True

DEBUG = False
DEBUG404 = not DEBUG


DATABASES = settings.DATABASES
STATIC_URL = '/static/'



# Parse database configuration from $DATABASE_URL

#DATABASES['default'] =  dj_database_url.config()
DATABASES['default']['engine'] =  dj_database_url.config()

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
#SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']



STATIC_URL = '/static/'

#STATIC_ROOT = os.path.join(settings.BASE_DIR, "static", "static_root")
STATIC_ROOT = 'statifiles'


STATICFILES_DIRS = [
    os.path.join(settings.BASE_DIR, "static","static_dirs"),
]

STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

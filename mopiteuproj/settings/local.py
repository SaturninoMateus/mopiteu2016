# coding: utf-8
import os
import dj_database_url

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
# Quick-start devstelopment settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '_)02%$k5six=6v2_3n7i_dczf+ns#2vm(mx2dhs_rfzy^yom7x'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
DEBUG404 = not DEBUG


#ALLOWED_HOSTS = ['127.0.0.1','www.mopiteu.com','mopiteu.heroku.com']
ALLOWED_HOSTS = ['*']




####################################################
#My setups
STATE_CHOICES = [
    ('LUANDA', 'Luanda'),
    ('BENGUELA', 'Benguela')
]

SERVICE_TYPE_CHOICES = [
    ('DELIVERY', 'Entrega'),
    ('PICKUP', 'Retirada'),
    ('DELIVERY&PICKUP', 'Entrega & Retirada')
]

ORDER_STATUS_CHOICES = [
    ('WAITING', 'Em espera'),
    ('DELIVERING', 'A caminho'),
    ('DELIVERED', 'Entregue'),
    ('CUSTOM', 'Personalizado'),
    ('FILED', 'Arquivado'),
]



SERVICE_TYPE_CHOICES_LIST = ['Entrega', 'Retirada', 'Entrega & Retirada']

CURRENCY = 'Kz'

#############
#Pusher Real time settings
PUSHER_APP_ID='196285'
PUSHER_KEY='22a581cdf0d22671bf00'
PUSHER_SECRET='d945972c3f1008703f98'
PUSHER_SSL=True
#############

##############
#Twilio SMS
# put your own credentials here
ACCOUNT_SID = "ACac68eaae2992298c2308a713c4a75a09"
AUTH_TOKEN = "fe83c721b6d4481832b0701efaf06a87"
TWILIO_NUMBER = "+14692771884"
##############



EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = 'mopiteu.ao'
EMAIL_HOST_PASSWORD = '859-mopiteu.ao16'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
"""
send_mail('Subject here', 'Here is the message.', 'from@example.com', ['to@example.com'], fail_silently=False)
"""





# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    #3rd apps
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'drealtime',
    'storages', #for amazon s3 service
    'pusher',



    #my apps
    'mopiteuapp',
    'users',
    'restaurants',
    'products',
    'carts',
    'newsletter',
    'ordersBeta',
]
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage' #Amazon s3

SITE_ID = 2


AUTHENTICATION_BACKENDS = (

    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',

    # `allauth` specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',
)


ACCOUNT_AUTHENTICATION_METHOD = "username_email"
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 7
ACCOUNT_SIGNUP_FORM_CLASS = 'users.forms.SignupForm'
LOGIN_REDIRECT_URL = '/'
DEFAULT_FROM_EMAIL ='admin@mopiteu.com'

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'drealtime.middleware.iShoutCookieMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'mopiteuproj.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
            ],
        },
    },
]

WSGI_APPLICATION = 'mopiteuproj.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

if DEBUG:

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }
    '''
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'mopiteudatabasebeta',
            'USER': 'admin1',
            'PASSWORD': 'admin051',
            'HOST': 'localhost',
            'PORT': '5432',
        }
    }
    '''
else:

    #http://stackoverflow.com/questions/28648695/migrate-django-development-database-sql3-to-heroku
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql_psycopg2",
            "NAME": "d7bk4519ut01j3",
            "USER": "tgemazcnyndvsd",
            "PASSWORD": "45-9QwM7xjblAY8RDszgEMe9Hd",
            "HOST": "ec2-54-83-194-117.compute-1.amazonaws.com",
            "PORT": "5432",
        }
    }





# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'pt-br'

#TIME_ZONE = 'UTC'
TIME_ZONE = 'Africa/Luanda'

USE_I18N = True

USE_L10N = True

USE_TZ = True




MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "static", "media")




# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/
#BASE_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_URL = '/static/'
#STATIC_ROOT = os.path.join(BASE_DIR, "static", "static_root")
STATIC_ROOT = 'staticfiles'


STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static", "static_dirs"),
]

#STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'



#http://127.0.0.1:8000/accounts/facebook/login/callback/
#http://localhost:8000/accounts/facebook/login/callback/
SOCIALACCOUNT_PROVIDERS = \
    {'facebook':
         {'METHOD': 'oauth2',
          'SCOPE': ['email', 'public_profile', 'user_friends'],
          'AUTH_PARAMS': {'auth_type': 'reauthenticate'},
          'FIELDS': [
              'id',
              'email',
              'name',
              'first_name',
              'last_name',
              'verified',
              'locale',
              'timezone',
              'link',
              'gender',
              'updated_time'],
          'EXCHANGE_TOKEN': True,
          'LOCALE_FUNC': lambda request: 'en_US',
          'VERIFIED_EMAIL': True,
          'VERSION': 'v2.4'}}

##############################
#BEGIN AMAZON S3 SETUP

AWS_ACCESS_KEY_ID = "AKIAJT3DWBAG27HL3RUQ"
AWS_SECRET_ACCESS_KEY = "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"


AWS_FILE_EXPIRE = 200
AWS_PRELOAD_METADATA = True
AWS_QUERYSTRING_AUTH = True

DEFAULT_FILE_STORAGE = 'mopiteuproj.utils.MediaRootS3BotoStorage'
#STATICFILES_STORAGE = 'mopiteuproj.utils.StaticRootS3BotoStorage'
AWS_STORAGE_BUCKET_NAME = 'mopiteu2016-bucket'
S3DIRECT_REGION = 'sa-east-1'
S3_URL = '//%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME
MEDIA_URL = '//%s.s3.amazonaws.com/media/' % AWS_STORAGE_BUCKET_NAME
MEDIA_ROOT = MEDIA_URL
#STATIC_URL = S3_URL + 'static/'
ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'

import datetime

date_two_months_later = datetime.date.today() + datetime.timedelta(2 * 365 / 12)
expires = date_two_months_later.strftime("%A, %d %B %Y 20:00:00 GMT")

AWS_HEADERS = {
    'Expires': expires,
    'Cache-Control': 'max-age=86400',
}

#END AMAZON S3 SETUP
##################################################################
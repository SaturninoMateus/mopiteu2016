"""mopiteuproj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include, patterns
from django.conf.urls.static import static
from django.contrib import admin
from mopiteuapp.views import Home, Contact
from newsletter.views import Newsletter
import os

urlpatterns = [
    url(r'^seliga/', admin.site.urls),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^restaurant/', include('restaurants.urls')),
    url(r'^restaurant/(?P<id>\d+)/(?P<slug>[\w-]+)/products/', include('products.urls')),
    #url(r'^restaurant/(?P<id>\d+)/(?P<slug>[\w-]+)/products/(?P<prod_id>\d+)/cart/', include('carts.urls')),
    url(r'^restaurant/(?P<id>\d+)/(?P<slug>[\w-]+)/products/cart/', include('ordersBeta.urls')),
    url(r'^$', Home.as_view()),
    url(r'^newsletter', Newsletter.as_view()),
    url(r'^dashboard/', include('dashboard.urls')),
    url(r'^contact/', Contact.as_view()),

]


if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG404:
    print "entrou debug404"
    print os.path.join(os.path.dirname(__file__), 'static', 'static_root')
    urlpatterns += patterns('',
    (r'^static/(?P<path>.*)$', 'django.views.static.serve',
    {'document_root': os.path.join(os.path.dirname(__file__), 'static', 'static_dirs')} ),
    )

    urlpatterns += patterns('',
    (r'^media/(?P<path>.*)$', 'django.views.static.serve',
     {'document_root': settings.MEDIA_ROOT, 'show_indexes':True}),
                            )
    print "entrou no debug404"
    print settings.MEDIA_ROOT


# coding: utf-8
from __future__ import unicode_literals
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render
from django.views.generic import View
from restaurants.models import Restaurant
from products.models import Product, Category
from django.shortcuts import get_object_or_404
from ordersBeta.models import OrderBeta
from django.core.mail import send_mail, EmailMessage
from django.template.loader import render_to_string, get_template
from django.template import Context
from django.conf import settings
from twilio.rest import TwilioRestClient
import pusher, time


import ast, sys



class Home(View):
    @method_decorator(login_required)
    def get(self,request, *args, **kwargs):
        params = {}

        if request.user.username not in 'admin':
            restaurant = get_object_or_404(Restaurant, email=request.user.email)
            params = {"restaurant":restaurant}
            print request.user.username

        #return render(request, 'dashboard/index.html', params)
        orders = OrderBeta.objects.filter(restaurant_id = restaurant.id).exclude(status="FILED").order_by("id")
        print orders
        params = {"orders":orders,"restaurant":restaurant}
        return render(request, 'dashboard/orders.html', params)


class OrdersView(View):
    @method_decorator(login_required)
    def get(self,request, *args, **kwargs):
        params = {}

        if request.user.username not in 'admin':
            restaurant = get_object_or_404(Restaurant, email=request.user.email)
            params = {"restaurant":restaurant}
            print request.user.username
            #print 'LOG: Fetching orders from %s...' %(restaurant.name)
            orders = OrderBeta.objects.filter(restaurant_id = restaurant.id).exclude(status="FILED").order_by("id")
            print orders
            #print 'LOG: Fetched from %s successfull.'%(restaurant.name)
            params = {"orders":orders,"restaurant":restaurant}


        return render(request, 'dashboard/orders.html', params)


    @method_decorator(login_required)
    def post(self,request, *args, **kwargs):
        params = {}
        the_status = ''
        print request.POST
        if request.user.username not in 'admin':
            restaurant = get_object_or_404(Restaurant, email=request.user.email)

            try:
                try:
                    pusher_client = pusher.Pusher(
                    app_id=settings.PUSHER_APP_ID,
                    key=settings.PUSHER_KEY,
                    secret=settings.PUSHER_SECRET,
                    ssl=settings.PUSHER_SSL,
                    )
                except:
                    pass

                rest_to = request.POST.get('rest_to')
                print rest_to
                if rest_to == 'abre':
                    restaurant.is_open = True
                    restaurant.save()
                    try:
                        pusher_client.trigger('status-'+restaurant.slug, 'is_open', {'message': ''})
                    except:
                        pass

                    return render(request, 'dashboard/orders.html', params)
                elif rest_to == 'fecha':
                    restaurant.is_open = False
                    restaurant.save()
                    print rest_to

                    try:
                        pusher_client.trigger('status-'+restaurant.slug, 'is_open', {'message': ''})
                    except:
                        pass
                    return render(request, 'dashboard/orders.html', params)

            except:
                pass



            try:
                id = int(request.POST.get("order_id"))
            except:
                pass

            try:
                proceedOrder = int(request.POST.get("proceedOrder"))
            except:
                pass

            try:
                deleteOrder = int(request.POST.get("deleteOrder"))
            except:
                pass

            order = get_object_or_404(OrderBeta, restaurant_id= restaurant.id, id=id)
            html = request.POST.get("msg")




            if proceedOrder:
                print order.status
                if order.status == "WAITING":
                    order.status = "DELIVERING"
                    the_status = "Saiu para entrega"
                    try:
                        client = TwilioRestClient(settings.ACCOUNT_SID, settings.AUTH_TOKEN)
                        msg = '''Estado do seu pedido #MPT%s: Saiu para entrega\nMOPITEU.com'''%(order.id)
                        print 'Twilio- Tentando enviar SMS...'
                        client.messages.create(
                        to="+244"+order.phone,
                        from_= settings.TWILIO_NUMBER,
                        body=msg,
                        )
                        time.delay(4) #delay of 3 seconds
                        print 'Twilio- SMS enviado com sucesso!'
                    except:
                        pass

                elif order.status == "DELIVERING":
                    order.status = "DELIVERED"
                    the_status = "Entregue"

                order.save()
                subject = 'MOPITEU - Pedido nº #MPT%s'%(order.id)
                msg = '''
                Estado do seu pedido: %s

                Atenciosamente,

                Serviço de Apoio ao Cliente MOPITEU.
                '''%(the_status)
                send_mail(subject, msg,'sac@mopiteu.com', [order.email], fail_silently=False)

                return render(request, 'dashboard/orders.html', params)

            elif deleteOrder:
                order.status = 'FILED'
                order.save()
                return render(request, 'dashboard/orders.html', params)
            #######################################################################
            #Different way to send email
            #http://masnun.com/2014/01/09/django-sending-html-only-email.html
            #Unicode concatenation issue: http://stackoverflow.com/questions/17052089/django-unicode-concatenation

            subject = 'MOPITEU - Pedido nº #MPT%s'%(order.id)
            html = html+'''
            <p>Atenciosamente,</p>
            Serviço de Apoio ao Cliente MOPITEU.
            '''
            msg = EmailMessage(subject, html, "sac@mopiteu.com", [order.email])
            msg.content_subtype = "html"
            order.status = "CUSTOM"
            order.save()
            msg.send()
            #######################################################################

            return render(request, 'dashboard/orders.html', params)




class ProductView(View):
    @method_decorator(login_required)
    def get(self,request, *args, **kwargs):
        params = {}
        if request.user.username not in 'admin':
            restaurant = get_object_or_404(Restaurant, email=request.user.email)
            products = Product.objects.filter(restaurant_id=restaurant.id).order_by("timestamp")
            print products
            categories = Category.objects.filter(active=True)

            params = {"restaurant":restaurant,"products":products, "categories":categories}
        return render(request, 'dashboard/products.html', params)


    @method_decorator(login_required)
    def post(self,request, *args, **kwargs):
        print request.POST
        restaurant = get_object_or_404(Restaurant, email=request.user.email)
        products = Product.objects.filter(restaurant_id=restaurant.id).order_by("timestamp")


        delete = request.POST.get("delete")
        if delete:
            try:
                id = request.POST.get("id")
                name = request.POST.get("name")
                p = get_object_or_404(Product,title=name,restaurant_id =restaurant.id, id=int(id))
                p.delete()
                return render(request, 'dashboard/products.html', {}) #só jajão, nao retorna nada

            except:
                pass



        name = request.POST.get('name')
        category = request.POST.get('category')
        active = request.POST.get('active')
        description = request.POST.get('description')
        price = request.POST.get('price')

        categories = Category.objects.filter(active=True, title = category)

        if active in 'yes':
            active = True
        else:
            active = False

        prod_id = request.POST.get('product-id')
        if prod_id:
            p = get_object_or_404(Product, id=int(prod_id))
            p.title = name
            p.description = description
            p.price = int(price)
            p.active = active
            p.restaurant = restaurant
            p.save()
            p.categories.add(categories[0].id)
            p.default = get_object_or_404(Category,title=category)
            p.save()
            return render(request, 'dashboard/products.html', {}) #só jajão, nao retorna nada


        p = Product.objects.create(title=name,description=description,price=int(price),active=active,restaurant=restaurant)
        p.save()
        p.categories.add(categories[0].id)
        p.default = categories[0]
        p.save()

        params = {"restaurant":restaurant,"products":products, "categories":categories}
        return render(request, 'dashboard/products.html', params)


class ProfileView(View):
    @method_decorator(login_required)
    def get(self,request, *args, **kwargs):
        restaurant = get_object_or_404(Restaurant, email=request.user.email)
        params = {"restaurant":restaurant}
        return render(request, 'dashboard/profile.html', params)
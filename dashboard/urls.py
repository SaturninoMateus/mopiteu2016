from django.conf.urls import url, include
from views import Home, OrdersView, ProductView, ProfileView


urlpatterns = [
    url(r'^$', Home.as_view(), name='dashboard_home_url'),
    url(r'^orders/$', OrdersView.as_view(), name='dashboard_orders_url'),
    url(r'^products/$', ProductView.as_view(), name='dashboard_products_url'),
    url(r'^profile/$', ProfileView.as_view(), name='dashboard_profile_url'),
]
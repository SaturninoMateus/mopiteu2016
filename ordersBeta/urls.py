from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from views import Order,Checkout

urlpatterns = [
    url(r'^$', Order.as_view(), name='ordersBeta'),
    url(r'^checkout/', Checkout.as_view(), name='checkout'),
]
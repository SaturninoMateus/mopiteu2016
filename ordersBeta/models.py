from __future__ import unicode_literals
from django.conf import settings
from django.db import models
from products.models import Product
from restaurants.models import Restaurant
from django.conf import settings


class Item(models.Model):
    product = models.OneToOneField(Product)
    qty = models.PositiveIntegerField(default=1)
    timestamp = models.DateTimeField(auto_now_add=True,auto_now=False)
    updated = models.DateTimeField(auto_now_add=False,auto_now=True)
    order = models.ForeignKey('OrderBeta')

    def __unicode__(self):
        return "%s-%s" %("item", self.product.title)













class CartItem(models.Model):
    item = models.ForeignKey(Product)
    order = models.ForeignKey('OrderBeta')
    qty = models.PositiveIntegerField(default=1)
    timestamp = models.DateTimeField(auto_now_add=True,auto_now=False)
    updated = models.DateTimeField(auto_now_add=False,auto_now=True)


    def __unicode__(self):
        return "%s-%s" %("item", self.item.title)


class OrderBeta(models.Model):
    restaurant = models.ForeignKey(Restaurant)
    name = models.CharField(max_length=20,null=False)
    last_name = models.CharField(max_length=20, null=False)
    phone = models.CharField(max_length=9, null=False)
    email = models.EmailField(null=False)
    city = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    delivery_day = models.CharField(max_length=9)
    pay_with = models.PositiveIntegerField()
    observations = models.TextField(max_length=1000)
    status = models.CharField(max_length=30,choices=settings.ORDER_STATUS_CHOICES, default='WAITING')

    timestamp = models.DateTimeField(auto_now_add = True, auto_now=False)
    updated = models.DateTimeField(auto_now_add = False, auto_now=True)

    items = models.ManyToManyField(Product, through=CartItem)



    def __unicode__(self):
        return "%s-%s" %(self.name, str(self.id))

    def get_items(self):
        items = CartItem.objects.filter(order=self)
        return items


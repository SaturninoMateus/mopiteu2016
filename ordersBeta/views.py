# coding: utf-8
from django.shortcuts import render
from django.views.generic import View
from django.shortcuts import get_object_or_404

from restaurants.models import Restaurant
from products.models import Product

from django.core.mail import send_mail
from models import OrderBeta, CartItem
from django.contrib.auth.models import User
from django.conf import settings

from django.template import RequestContext
from drealtime import iShoutClient
ishout_client = iShoutClient()
import ast, time
import pusher, os, requests
from twilio.rest import TwilioRestClient


class Order(View):
    def get(self,request, *args, **kwargs):
        slug = kwargs.get("slug")

        '''ishout_client.emit(
            2,
            'notifications',
            data = {'msg':"msgggggggg"},
        )'''
        print 'okokokokok'

        serviceType = request.GET.get("service")
        is_delivery = False
        is_takeaway = False

        if serviceType in "delivery":
            is_delivery = True
        elif serviceType in "takeaway":
            is_takeaway = True


        restaurant = Restaurant.objects.filter(slug=slug)[0]
        theUrl = "/restaurant/"+str(restaurant.id)+"/"+restaurant.slug+"/products/?edit=true"
        thePostUrl = "/restaurant/"+str(restaurant.id)+"/"+restaurant.slug+"/products/cart/"
        params = {"restaurant":restaurant, "is_delivery":is_delivery, "is_takeaway":is_takeaway, "theUrl":theUrl, "thePostUrl":thePostUrl}

        print 'ok'


        return render(request, 'cart/cart.html', params)

    def post(self, request, *args, **kwargs):
        print 'ok'
        slug = kwargs.get("slug")
        restaurant = Restaurant.objects.filter(slug=slug)[0]

        ################################
        #Fetching billing data
        #convert string to dict
        order_post = ast.literal_eval(request.POST.get("order"))
        info_post = ast.literal_eval(request.POST.get("info"))

        print order_post
        print info_post

        name = info_post["firstname"]
        lastname = info_post["lastname"]
        phone = info_post["tel"]
        email = info_post["email"]
        city = info_post["city"]
        address = info_post["address"]
        delivery_day = info_post["delivery_day"]
        pay_with = info_post["paywith"]
        obs = info_post["notes"]

        restaurant = get_object_or_404(Restaurant, slug=slug)

        order = OrderBeta.objects.create(
            restaurant=restaurant,
            name=name,last_name=lastname,
            phone=phone,email=email,
            city=city,address=address,
            delivery_day=delivery_day,
            pay_with=pay_with,observations=obs,
        )
        order.save()
        for key in order_post.keys():
            if key not in 'total':
                product = get_object_or_404(Product, id=int(key))
                print 'passou1'
                cart_item = CartItem.objects.create(item=product,qty=int(order_post[key]), order=order)
                cart_item.save()
                print 'passou2'

        subject = 'MOPITEU - Pedido nº #MPT%s'%(order.id)
        msg = '''
        Prezado %s %s,

        O seu pedido foi recebido com sucesso e será processado o mais breve possível!

        Atenciosamente,

        Serviço de Apoio ao Cliente MOPITEU.
        '''%(order.name, order.last_name)
        send_mail(subject, msg,'sac@mopiteu.com', [email], fail_silently=False)
        print 'Email enviado com sucesso!'
        try:
            #Send push notification to manager dashboard page
            pusher_client = pusher.Pusher(
                app_id=settings.PUSHER_APP_ID,
                key=settings.PUSHER_KEY,
                secret=settings.PUSHER_SECRET,
                ssl=settings.PUSHER_SSL,
                )
            pusher_client.trigger('order-'+restaurant.slug, 'new_order', {'message': ''})
            ################################
        except:
            pass


        try:
            client = TwilioRestClient(settings.ACCOUNT_SID, settings.AUTH_TOKEN)
            msg = '''%s %s,\nO seu pedido foi recebido com sucesso\nMOPITEU.com'''%(order.name, order.last_name)
            print 'Twilio- Tentando enviar SMS...'
            client.messages.create(
	        to="+55"+order.phone,
	        from_= settings.TWILIO_NUMBER,
	        body=msg,
            )
            time.delay(4) #delay of 3 seconds
            print 'Twilio- SMS enviado com sucesso!'
        except:
            pass

        #the restaurant email must the the same as the user email
        #user = get_object_or_404(User, email=restaurant.email)
        '''
        ishout_client.emit(
            user.id,
            'notifications',
            data = {'msg':order.name},
        )'''

        return render(request, 'cart/checkout.html',{})



class Checkout(View):
    def get(self, request, *args, **kwargs):
        slug = kwargs.get("slug")
        restaurant = Restaurant.objects.filter(slug=slug)[0]
        params = {"restaurant":restaurant}
        return render(request, 'cart/checkout.html',params)
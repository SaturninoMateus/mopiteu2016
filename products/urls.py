from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from views import ProductView, ProductDetails

urlpatterns = [
    url(r'^$', ProductView.as_view(), name='product_view_url'),
    url(r'^(?P<id>\d+)/$', ProductDetails.as_view(), name='product_details_url'),
]
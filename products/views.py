from django.shortcuts import render
from django.views.generic import View,TemplateView
from django.shortcuts import get_object_or_404, get_list_or_404
from products.models import Product, Variation, Category
from restaurants.models import Restaurant, RestaurantAddress
from django.conf import settings
from carts.models import Cart,CartItem
import weakref



class MyProduct(object):
    def __init__(self, category, name, title, price):
        self.category = category
        self.name  = name
        self.price  = price





class ProductView(View):

    def get(self, request, slug, id):
        print slug
        print id
        edit = request.GET.get("edit")
        if edit == "true":
            edit = "true"
        else:
            edit = "false"

        theUrl = "/restaurant/"+id+"/"+slug+"/products/"




        categories = get_list_or_404(Category)
        products = get_list_or_404(Product, restaurant_id=id, active=True)
        restaurant = Restaurant.objects.filter(id=id)[0]

        categories_list = []
        #############################
        #dummy way to find all categories from a specific restaurant
        for p in products:
            for c in categories:
                if p in c.product_set.all() and c not in categories_list:
                    categories_list.append(c)
                    break
        ##############################

        query = request.GET.get("q")

        #filter products by a specific category
        if query:
            categories = Category.objects.filter(title__icontains=query)
            products = get_list_or_404(Product, categories__in=categories, restaurant_id=id, active=True)
            flag = query
            params = {"products":products,"restaurant_slug":slug, "categories":categories_list, "flag":query, "restaurant":restaurant, "rest_id":int(id),"edit":edit, "thrUrl":theUrl}
            return render(request, 'product/detail_page.html', params)




        ###############################################################
        #######This part add product to the CartBetaApp
        item_id = request.GET.get("item")
        qty = request.GET.get("value")


        ###############################################################






        params = {"products":products,"restaurant_slug":slug, "categories":categories_list, "restaurant":restaurant, "rest_id":int(id), "edit":edit, "theUrl":theUrl}
        print 'aqui mesmo'
        return render(request, 'product/detail_page.html', params)


class ProductDetails(View):

    def get(self, request, id, slug):

        restaurant = get_object_or_404(Restaurant, slug=slug)
        product = get_object_or_404(Product, id=id, restaurant=restaurant)#.filter(slug=slug)
        params = {"product":product, "currency":settings.CURRENCY, "slug":restaurant.slug}
        return render(request, 'product/product_details.html', params)
